#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{

	FILE* my_file = fopen("IRIS_Data.csv", "r");

	size_t count;
	int column = 5;
	int row = 100;

	float a[100],b[100],c[100],d[100],e[100];
	float data[row][column];
	for (count = 0; count < sizeof(a)/sizeof(a[0]); ++count)
	{
		int got = fscanf(my_file, " %f,%f,%f,%f,%f",&a[count], &b[count], &c[count], &d[count], &e[count]);
		printf("%d\n",count );
		if (got == -1) break; // wrong number of tokens - maybe end of file
	}

	fclose(my_file);

	for (int i = 0; i < count; ++i)
	{
		printf("%d. %0.1f %0.1f %0.1f %0.1f %g \n",i+1, a[i], b[i], c[i], d[i], e[i]);
	}
	return 0;
}